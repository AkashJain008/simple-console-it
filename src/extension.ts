// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "simple-console-it" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('simple-console-it.simpleConsoleIt', () => {
		// The code you place here will be executed every time your command is executed
		const editor = vscode.window.activeTextEditor;
		if(!editor) {
			vscode.window.showInformationMessage('Editor does not exist!');
			return;
		}
		const text = editor.document.getText(editor.selection);
		if(!text) {
			vscode.window.showInformationMessage('Please select any variable.');
			return;
		}
		if(text && (text.indexOf(',') > -1 || text.indexOf(';') > -1 || text.indexOf('=') > -1)) {
			vscode.window.showErrorMessage(`Invalid variable selection- ${text}`);
			return;
		}
		vscode.window.showInformationMessage(`Selected variable- ${text}`);
		editor.edit((edit)=> {
			let lineNumber = new vscode.Position(editor.selection.end.line + 1,0);
			edit.insert(lineNumber,`console.log("${text}", JSON.stringify(${text}));\n`);
		});
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
