# Simple console it (Ctrl+Alt+p)

A new extension for adding a console log automatically on any variable.

## Features

To print the variables with the help of console log function.

It eases the debugging process.

It can print any datatype variable on the console.

It will be like this:

![feature X](images/simple-console.gif)

## Requirements
It will only work with TypeScript/JavaScript extension files.

## Extension Settings
None

## Quick Start

Install the extension.

Select the variable, which you want to print on console.

Press `Ctrl+Alt+p` (Windows, Linux) or `Ctrl+Alt+p` (mac OS) to generate the console log. 




